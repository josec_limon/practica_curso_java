import Vue from 'vue'
import App from './App.vue'
import './filtro-global';

Vue.config.productionTip = false

new Vue({
    render: function(h) { return h(App) },
}).$mount('#app')